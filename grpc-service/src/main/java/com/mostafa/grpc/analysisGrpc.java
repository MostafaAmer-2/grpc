package com.mostafa.grpc;

import java.io.IOException;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.15.0)",
    comments = "Source: analysis.proto")
public final class analysisGrpc {

  private analysisGrpc() {}

  public static final String SERVICE_NAME = "analysis";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.mostafa.grpc.Analysis.AnalyzeRequest,
      com.mostafa.grpc.Analysis.APIResponse> getAnalyzeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "analyze",
      requestType = com.mostafa.grpc.Analysis.AnalyzeRequest.class,
      responseType = com.mostafa.grpc.Analysis.APIResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.mostafa.grpc.Analysis.AnalyzeRequest,
      com.mostafa.grpc.Analysis.APIResponse> getAnalyzeMethod() {
    io.grpc.MethodDescriptor<com.mostafa.grpc.Analysis.AnalyzeRequest, com.mostafa.grpc.Analysis.APIResponse> getAnalyzeMethod;
    if ((getAnalyzeMethod = analysisGrpc.getAnalyzeMethod) == null) {
      synchronized (analysisGrpc.class) {
        if ((getAnalyzeMethod = analysisGrpc.getAnalyzeMethod) == null) {
          analysisGrpc.getAnalyzeMethod = getAnalyzeMethod = 
              io.grpc.MethodDescriptor.<com.mostafa.grpc.Analysis.AnalyzeRequest, com.mostafa.grpc.Analysis.APIResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "analysis", "analyze"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mostafa.grpc.Analysis.AnalyzeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mostafa.grpc.Analysis.APIResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new analysisMethodDescriptorSupplier("analyze"))
                  .build();
          }
        }
     }
     return getAnalyzeMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static analysisStub newStub(io.grpc.Channel channel) {
    return new analysisStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static analysisBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new analysisBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static analysisFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new analysisFutureStub(channel);
  }

  /**
   */
  public static abstract class analysisImplBase implements io.grpc.BindableService {

    /**
     */
    public void analyze(com.mostafa.grpc.Analysis.AnalyzeRequest request,
        io.grpc.stub.StreamObserver<com.mostafa.grpc.Analysis.APIResponse> responseObserver) throws IOException {
      asyncUnimplementedUnaryCall(getAnalyzeMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getAnalyzeMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.mostafa.grpc.Analysis.AnalyzeRequest,
                com.mostafa.grpc.Analysis.APIResponse>(
                  this, METHODID_ANALYZE)))
          .build();
    }
  }

  /**
   */
  public static final class analysisStub extends io.grpc.stub.AbstractStub<analysisStub> {
    private analysisStub(io.grpc.Channel channel) {
      super(channel);
    }

    private analysisStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected analysisStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new analysisStub(channel, callOptions);
    }

    /**
     */
    public void analyze(com.mostafa.grpc.Analysis.AnalyzeRequest request,
        io.grpc.stub.StreamObserver<com.mostafa.grpc.Analysis.APIResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAnalyzeMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class analysisBlockingStub extends io.grpc.stub.AbstractStub<analysisBlockingStub> {
    private analysisBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private analysisBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected analysisBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new analysisBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.mostafa.grpc.Analysis.APIResponse analyze(com.mostafa.grpc.Analysis.AnalyzeRequest request) {
      return blockingUnaryCall(
          getChannel(), getAnalyzeMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class analysisFutureStub extends io.grpc.stub.AbstractStub<analysisFutureStub> {
    private analysisFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private analysisFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected analysisFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new analysisFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.mostafa.grpc.Analysis.APIResponse> analyze(
        com.mostafa.grpc.Analysis.AnalyzeRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getAnalyzeMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_ANALYZE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final analysisImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(analysisImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_ANALYZE:
          try {
            serviceImpl.analyze((Analysis.AnalyzeRequest) request,
                (io.grpc.stub.StreamObserver<Analysis.APIResponse>) responseObserver);
          } catch (IOException e) {
            e.printStackTrace();
          }
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class analysisBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    analysisBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.mostafa.grpc.Analysis.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("analysis");
    }
  }

  private static final class analysisFileDescriptorSupplier
      extends analysisBaseDescriptorSupplier {
    analysisFileDescriptorSupplier() {}
  }

  private static final class analysisMethodDescriptorSupplier
      extends analysisBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    analysisMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (analysisGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new analysisFileDescriptorSupplier())
              .addMethod(getAnalyzeMethod())
              .build();
        }
      }
    }
    return result;
  }
}
