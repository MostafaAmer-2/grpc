package com.mostafa.server;

import com.mostafa.service.AnalysisService;
import com.mostafa.service.SearchForCountryService;
import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class GRPCServer {

    public static void main(String args[]) throws IOException, InterruptedException {

        Server server = ServerBuilder.forPort(9091).addService(new AnalysisService()).addService(new SearchForCountryService()).build();

        server.start();

        System.out.println("Server started on " + server.getPort());

        server.awaitTermination();

    }

}