package com.mostafa.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.custom.CustomAnalyzer.Builder;
import org.apache.lucene.analysis.util.FilesystemResourceLoader;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

public class BusinessPartnerAnalyzerFactory extends AnalyzerFactory {

	
	@Override
	public CustomAnalyzer GetCountryAnalyzer(String synonymsPath) throws IOException {

		File f = new File(System.getProperty("user.dir"));
		FilesystemResourceLoader resourceLdr = new FilesystemResourceLoader(f.toPath());

		Builder gpAnalyzer = CustomAnalyzer.builder(resourceLdr).withTokenizer("standard").addTokenFilter("lowercase");
		gpAnalyzer.addTokenFilter("germannormalization").addTokenFilter("asciifolding");
		gpAnalyzer.addTokenFilter("synonym", "synonyms", synonymsPath);

		return gpAnalyzer.build();
	}

	public static Directory makeIndex(Analyzer analyzer) throws IOException {

		Directory index = new RAMDirectory();
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter w = new IndexWriter(index, config);

		String csvFile = "src/main/resources/data.csv"; //path for data
		BufferedReader br = null;
		String row = "";
		try {
			br = new BufferedReader(new FileReader(csvFile));
			while ((row = br.readLine()) != null) {
				String[] rowData = row.split(",");
				addDoc(w, rowData[0], rowData[1], rowData[2], rowData[3], rowData[4], rowData[5]
						,rowData[6], rowData[7], rowData[8], rowData[9], rowData[10]);
			}
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		w.close();

		return index;

	}

	private static void addDoc(IndexWriter w, String id, String pid, String lcid,
							   String name, String street, String zip, String city,
							   String country,  String traffiqxID, String created,
							   String timestamp) throws IOException {
		Document doc = new Document();
		doc.add(new TextField("id", id, Field.Store.YES));
		doc.add(new TextField("pid", pid, Field.Store.YES));
		doc.add(new TextField("lcid", lcid, Field.Store.YES));
		doc.add(new TextField("name", name, Field.Store.YES));
		doc.add(new TextField("street", street, Field.Store.YES));
		doc.add(new TextField("zip", zip, Field.Store.YES));
		doc.add(new TextField("city", city, Field.Store.YES));
		doc.add(new TextField("country", country, Field.Store.YES));
		doc.add(new TextField("traffiqxID", traffiqxID, Field.Store.YES));
		doc.add(new TextField("created", created, Field.Store.YES));
		doc.add(new TextField("timestamp", timestamp, Field.Store.YES));
		w.addDocument(doc);
	}

}
