package com.mostafa.utils;

import java.io.IOException;

import org.apache.lucene.analysis.custom.CustomAnalyzer;

public abstract class AnalyzerFactory {

	public abstract CustomAnalyzer GetCountryAnalyzer(String synonymsPath) throws IOException;

}
