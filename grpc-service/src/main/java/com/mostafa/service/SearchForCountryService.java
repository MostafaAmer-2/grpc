package com.mostafa.service;

import com.mostafa.utils.BusinessPartnerAnalyzerFactory;
import com.mostafa.grpc.Search;
import com.mostafa.grpc.searchGrpc;
import io.grpc.stub.StreamObserver;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;

import java.io.IOException;

public class SearchForCountryService extends searchGrpc.searchImplBase {
	
	@Override
	public void searchForCountry(Search.SearchRequest request, StreamObserver<com.mostafa.grpc.Search.APISearchResponse> responseObserver) throws IOException, ParseException {
	      
		String searchKeyword = request.getCountryName();
		Search.APISearchResponse.Builder response = Search.APISearchResponse.newBuilder();
		
		BusinessPartnerAnalyzerFactory factory = new BusinessPartnerAnalyzerFactory();
		CustomAnalyzer y = factory.GetCountryAnalyzer("src/main/resources/synonyms.txt"); //path for synonyms

		Directory index = factory.makeIndex(y);


		QueryParser queryParser = new QueryParser("country", y);
		Query searchQuery = queryParser.parse(searchKeyword);
		// search
		int hitsPerPage = 10;
		IndexReader reader = DirectoryReader.open(index);
		IndexSearcher searcher = new IndexSearcher(reader);
		TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
		searcher.search(searchQuery, collector);
		ScoreDoc[] hits = collector.topDocs().scoreDocs;

		// display results
		StringBuilder s = new StringBuilder();
		s.append("Found ").append(hits.length).append(" hits.").append('\n');
		for (int i = 0; i < hits.length; ++i) {
			int docId = hits[i].doc;
			Document d = searcher.doc(docId);
			s.append("ID: ").append(d.get("id")).append(", Name: ").append(d.get("name")).append(", country: ").append(d.get("country")).append("\n");
		}

		reader.close();
		
		response.setResponseMessage2(s.toString());

		responseObserver.onNext(response.build());
		responseObserver.onCompleted();
		
	    }

  




}