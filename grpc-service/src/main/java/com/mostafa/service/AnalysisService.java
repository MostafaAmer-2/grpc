package com.mostafa.service;

import com.mostafa.grpc.Analysis;
import com.mostafa.grpc.analysisGrpc;
import io.grpc.stub.StreamObserver;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.WhitespaceAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AnalysisService extends analysisGrpc.analysisImplBase {

    @Override
    public void analyze(Analysis.AnalyzeRequest request, StreamObserver<Analysis.APIResponse> responseObserver) throws IOException {
        String sentence = request.getSentenceToAnalyze();
        Analysis.AnalyzeRequest.Analyzer analyzer = request.getAnalyzer();

        Analysis.APIResponse.Builder response = Analysis.APIResponse.newBuilder();

        Analyzer analyzerToBeUsed = new StandardAnalyzer(); //standard analyzer set as default value
        if(analyzer==Analysis.AnalyzeRequest.Analyzer.STANDARD){
            analyzerToBeUsed=new StandardAnalyzer();
        }
        else if(analyzer==Analysis.AnalyzeRequest.Analyzer.STOP){
            analyzerToBeUsed=new StopAnalyzer();
        }
        else if(analyzer==Analysis.AnalyzeRequest.Analyzer.SIMPLE){
            analyzerToBeUsed=new SimpleAnalyzer();
        }
        else if(analyzer==Analysis.AnalyzeRequest.Analyzer.WHITESPACE){
            analyzerToBeUsed=new WhitespaceAnalyzer();
        }

        List<String> result = applyAnalyzer(sentence, analyzerToBeUsed);
        response.setResponseMessage(result.toString());

        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }


    private List<String> applyAnalyzer(String text, Analyzer analyzer) throws IOException{
        List<String> result = new ArrayList<String>();
        TokenStream tokenStream = analyzer.tokenStream("", text);
        CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
        tokenStream.reset();
        while(tokenStream.incrementToken()) {
            result.add(attr.toString());
        }
        return result;
    }

}