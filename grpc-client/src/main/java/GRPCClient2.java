import com.mostafa.grpc.Analysis;
import com.mostafa.grpc.Search;
import com.mostafa.grpc.analysisGrpc;
import com.mostafa.grpc.searchGrpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;


public class GRPCClient2 {

    public static void main(String[] args){

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9091).usePlaintext().build();

        searchGrpc.searchBlockingStub stub =  searchGrpc.newBlockingStub(channel);

        String sentence = "germany";
        Search.SearchRequest request = Search.SearchRequest.newBuilder().setCountryName(sentence).build();

        Search.APISearchResponse response = stub.searchForCountry(request);
        System.out.println("Response: "+ response.getResponseMessage2());

    }

}