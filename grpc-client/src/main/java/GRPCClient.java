import com.mostafa.grpc.Analysis;
import com.mostafa.grpc.analysisGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;


public class GRPCClient {

    public static void main(String[] args){

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9091).usePlaintext().build();

        //stub from proto file

        analysisGrpc.analysisBlockingStub stub =  analysisGrpc.newBlockingStub(channel);

        String sentence = "This is just a simple demo for analysis";
        Analysis.AnalyzeRequest.Analyzer analyzer = Analysis.AnalyzeRequest.Analyzer.STANDARD;
        Analysis.AnalyzeRequest request = Analysis.AnalyzeRequest.newBuilder().setSentenceToAnalyze(sentence).setAnalyzer(analyzer).build();

        Analysis.APIResponse response = stub.analyze(request);

        System.out.println("Analyzer used: "+ analyzer.toString());
        System.out.println("Original Sentence: "+ sentence);
        System.out.println("Tokens: "+ response.getResponseMessage());

    }

}